var progressElm=$("<div>da</div>");
$("#am_widget_Farm").before(progressElm);
var total=$(".farm_icon_a").length -1;

var switched_village = false;

$("[class^='report_']").each(function(index, obj){setTimeout(function(){
var da =    progressElm.html(index+1+" / "+total);

var totals=$(".farm_icon_a").length -1;
if((Number((da[0].outerText).replace(/\s+/, "").split("/")[0])+totals == total+1) == false){location.reload(true)}


    $("tbody:has('> tr > td> .farm_icon_a')");
	$("tbody:has('> tr > td> .farm_icon_b')");
	$("tr:has('> #spear')");

   var arrA = document.querySelectorAll("form>table")[0].querySelectorAll("tr")[1].querySelectorAll("input:not([type=hidden])");
   var arrB = document.querySelectorAll("form>table")[0].querySelectorAll("tr")[3].querySelectorAll("input:not([type=hidden])");
	var arrHome = $("tr:has('> #spear') .unit-item");


	var modelA = $.map(arrA, function(o, i){return parseInt(o.value)});
	var modelB = $.map(arrB, function(o, i){return parseInt(o.value)});
	var atHome = $.map(arrHome, function(o, i){return parseInt(o.innerHTML)});

	var canUseA = $.grep(atHome, function(o, i){return o < modelA[i]}).length ==0;
	var canUseB = $.grep(atHome, function(o, i){return o < modelB[i]}).length ==0;
    if (!(canUseB||canUseA))
    {
    	if(!switched_village)
    	{
        	switched_village = true;
    		setInterval(function(){
                location.reload(true)
    		}, 300)
    	}
    	return;
    }

    var resHTML = obj.cells[5].innerHTML;
    var haul_HTML = obj.cells[2].innerHTML;
    var full = false;


    function farm_a()
    {
    	if(canUseA){
        	$("[class$='farm_icon_a']", obj)[0].click();
        	console.log("farmed with a");
    	}
    	else
    		farm_b();
    };

    function farm_b()
    {
    	if(canUseB){
		    $("[class$='farm_icon_b']", obj)[0].click();
		    console.log("farmed with b");
		}
		else
    		farm_a();
    };

    function farm_c()
    {
        $("[class$='farm_icon_c']", obj)[0].click();
        console.log("farmed with c");
    };

    function farm_a_or_b()
    {

        if (haul_HTML){
            if (haul_HTML[haul_HTML.indexOf(".png") - 1] == "1")
                full = true;
        };


        if (full)
            farm_b();
        else
          	farm_a();
    };



    if ($("[class$='farm_icon_c']", obj)[0])
    {
        if(resHTML.indexOf(".") > 0)
            farm_c();
        else
            farm_a_or_b();
    }
    else
        farm_a_or_b();

}, index*220+Math.random()*10)});