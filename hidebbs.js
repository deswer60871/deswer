var FMmapOverlay;
var LA_ids=[];
var toToggleBack = [];
var sitter = "";
var depthMax = 3;
var popupWasClosed = false;
var loadingLAstuff = false;
var fmHideLASettings = {hidePlayers:true, replaceFilters:true};
var countapikey = "hideBarbsMap";
/******PROGRAM VARS**********/

// Initialize Map Barbs Only
function main(){
    hitCountApi();
    if($("#fm_popup_container").length){
        UI.ErrorMessage("Script has already been loaded, reload the page before calling it again");
        return;
    }
    let sitterQuery = window.location.search.match(/t=\d+/g);
    if(sitterQuery)
        sitter = sitterQuery;
    if(window.location.href.indexOf("screen=map")==-1)
    {
        UI.ErrorMessage("Script must be run in map");
        window.location.href = window.location.pathname+ `?${sitter?sitter+"&":""}screen=map`;
        return;
    }

    //setting new map functions
    FMmapOverlay = TWMap;
    FMmapOverlay.mapHandler._FMspawnSector = FMmapOverlay.mapHandler.spawnSector;

    FMmapOverlay.popup.__FMisAwayFromContext = FMmapOverlay.popup._isAwayFromContext;
    TWMap.popup._isAwayFromContext = function(x, y){
        let village  = FMmapOverlay.villages[x*1000 + y];
        if(village){
            if(toHideQuestionMark(village)){
                return false;
            }
        }

        return FMmapOverlay.popup.__FMisAwayFromContext(x,y);
    };

    setHTML();
    getFirstFarmPage();
}

function hitCountApi(){
    $.getJSON(`https://api.countapi.xyz/hit/Deswer/${countapikey}`, function(response) {
        console.log(`This script has been run ${response.value} times`);
    });
}
//class="popup_box show"
//class="popup_box_content"

function setHTML(){
    let html =`
    <div id="fm_popup_container" class="fm_popup">
        <div>
            <a class="popup_box_close tooltip-delayed fm_popup" id="fm_popup_cross" href="javascript:void(0)"> 
            </a>
            <div id="fm_popup_content" class="fm_popup">
                <h3 class ="fm_centered"> Hide barbarians in LA</h3>
                <p>
                    <input type="checkbox" id="hide_players" class="fm_checkbox_LAHider" checked>
                    <label for="hide_players"><b>Hide player villages</b></label>
                </p>
                <p>
                    <input type="checkbox" id="changeLAFilters" class="fm_checkbox_LAHider" checked>
                    <label for="changeLAFilters"><b>Change LA filters</b></label>
                </p>
                <p>
                    <input class="btn" id="fm_reloadMap" type="submit" value="Reload map">
                </p>
            </div>
        </div>
    </div>
    <style>
        #fm_popup_container {
            border: 19px solid #804000;
            -moz-border-image: url("/graphic/popup/border.png") 9 19 19 19 repeat;
            -webkit-border-image: url("/graphic/popup/border.png") 9 19 19 19 repeat;
            -o-border-image: url("/graphic/popup/border.png") 19 19 19 19 repeat;
            border-image: url("/graphic/popup/border.png") 19 19 19 19 repeat;
            display: block;
            position: fixed;
            top: 8%;
            left: 70%;
            z-index: 14000;
        }
        #fm_popup_content {
            min-width: 200px;
            min-height: 120px;            
            height:100%;
            background-image: url('/graphic/popup/content_background.png');
        }
        .fm_centered {
            text-align: center;
        }
    </style>`;
    $("body").append(html);
    $("#fm_popup_cross").click(closePopup);
    $("#fm_reloadMap").click(reloadMap);
    $(".fm_checkbox_LAHider").on("change", setCheckboxValuesCache);
    $("#fm_popup_container").draggable();
    getCheckboxValuesCache();
    addAuthor("#fm_popup_content");
}

function toHideQuestionMark(village){
    return LA_ids.includes(village.id) || (fmHideLASettings.hidePlayers?village.owner!=0:false);
}

function getCheckboxValuesCache(){
    console.log("getting checkbox values");
    let cachedSettings = window.localStorage.getItem("fmHideLASettings");
    fmHideLASettings = cachedSettings ? JSON.parse(cachedSettings) : {hidePlayers:true, replaceFilters:true};
    $("#hide_players").prop("checked", fmHideLASettings.hidePlayers);
    $("#changeLAFilters").prop("checked", fmHideLASettings.replaceFilters);
}

function setCheckboxValuesCache(){
    console.log("setting checkbox values");
    fmHideLASettings.hidePlayers = $("#hide_players")[0].checked;
    fmHideLASettings.replaceFilters = $("#changeLAFilters")[0].checked;
    window.localStorage.setItem("fmHideLASettings", JSON.stringify(fmHideLASettings));
}

function reloadMap(){
    console.log("reloadingMap");
    if(loadingLAstuff){
        UI.ErrorMessage("Script is already loading map, please wait for it to finish before reloading or reload the page");
        return;
    }
    getFirstFarmPage();
}

function addAuthor(cointainerSelector){
    let authorHTML = `
    <div style="border: 1px solid #804000; padding: 5px;">
        <b>Script:</b> <a href="https://forum.tribalwars.net/index.php?threads/hide-barbarians-in-la.286468/" target="_blank">  Hide barbarians in LA</a>
        <br>
        <b>Author:</b> <a href="https://forum.tribalwars.net/index.php?members/the-quacks.124200/" target="_blank">fmthemaster The Quacks dve devky</a>
    </div>`;
    $(cointainerSelector).append(authorHTML);

}

function closePopup(){
    $('#fm_popup_container').remove();
    FMmapOverlay.mapHandler.spawnSector = FMmapOverlay.mapHandler._FMspawnSector;
    FMmapOverlay.popup._isAwayFromContext = FMmapOverlay.popup.__FMisAwayFromContext;
    FMmapOverlay.reload();
    popupWasClosed = true;
}

function hideVillages() {
    if(popupWasClosed){
        return;
    }
    if(toToggleBack.length ==0)
        loadingLAstuff = false;
    FMmapOverlay.reload();
    TWMap.mapHandler.spawnSector = spawnSectorReplacer;
    FMmapOverlay.villages = TWMap.villages;

    const villagesData = FMmapOverlay.villages;

    for (var key in villagesData) {
        if (villagesData.hasOwnProperty(key)) {
            const currentVillage = villagesData[key];
            doMapFiltering(currentVillage);
        }
    }
}

// Override Map Sector Spawn
function spawnSectorReplacer(data, sector) {
    FMmapOverlay.mapHandler._FMspawnSector(data, sector);
    var beginX = sector.x;
    var endX = beginX + FMmapOverlay.mapSubSectorSize;
    var beginY = sector.y;
    var endY = beginY + FMmapOverlay.mapSubSectorSize;
    for(var x =beginX; x < endX; x++)
    {
        for(var y =beginY; y < endY; y++)
        {
            var v = FMmapOverlay.villages[x * 1000 + y];
            if (v) {
                doMapFiltering(v);
            }
        }
    }
}

// Helper: Filter out villages
function doMapFiltering(village) {
    if(toHideQuestionMark(village)){
        $('#map_container > div:first-child').css({
            display: 'none',
        });
        $(`[id="map_village_${village.id}"]`).css({
            display: 'none',
        });
        $(`[id="map_icons_${village.id}"]`).css({
            display: 'none',
        });
        $(`[id^="map_cmdicons_${village.id}"]`).css({
            display: 'none',
        });
        $('#map_village_undefined').css({
            display: 'none',
        });
        $('img[src="/graphic/map/reserved_player.png"]').css({
            display: 'none',
        });
        $('img[src="/graphic/map/reserved_team.png"]').css({
            display: 'none',
        });
        $('#map canvas').css({
            display: 'none',
        });
    }
}

function startLoader(length)
{
    let width = $("#contentContainer")[0].clientWidth;
    $("#contentContainer").eq(0).prepend(`
    <div id="progressbar" class="progress-bar">
        <span class="count label">0/${length}</span>
        <div id="progress"><span class="count label" style="width: ${width}px;">0/${length}</span></div>
    </div>`);
}

function loaded(num, length, action)
{
    $("#progress").css("width", `${(num + 1) / length * 100}%`);
    $(".count").text(`${action} ${(num + 1)} / ${length}`);
    if(num+1==length)
        endLoader();
}

function endLoader()
{
    if($("#progressbar").length > 0)
        $("#progressbar").remove();
}

function executeQueue(queue, timeout, {loadText="",callback=()=>null}){
    if(queue.length){
        startLoader(queue.length);
        $.each(queue,(key, func)=>{
            setTimeout(()=>{
                loaded(key, queue.length, loadText);
                if(key==queue.length -1){
                    setTimeout(callback, timeout);
                    endLoader();
                }
                func();
            }, timeout*key);
        });
    }
    else
        setTimeout(callback, timeout);
}

async function getFirstFarmPage(){
    loadingLAstuff = true;
    $.get(`/game.php?${sitter?sitter+"&":""}village=${game_data.village.id}&screen=am_farm&Farm_page=0`, async (data)=> {
            const parser = new DOMParser();
            const doc= await parser.parseFromString(data, "text/html");
            let currentCheckBoxValues = Object.assign({},...$("#plunder_list_filters", doc).find("input[type=checkbox]", doc).map((key,obj)=>{return{[obj.id]:obj};}));
            // console.log(currentCheckBoxValues);
            let postGetQueue = [];

            let toggleBox =(key, url, val)=>{
                let data = `extended=1&target_screen=am_farm&${key}=${val}&h=${csrf_token}`;
                console.log(key, url, data);
                TribalWars.post(url,null,{extended:0+true, target_screen:"am_farm", [key]:val});
            };
            let setToggleFunction =(checkboxName, key, url, intendedValue)=>{
                console.log(checkboxName, url, intendedValue);
                if(fmHideLASettings.replaceFilters && currentCheckBoxValues[checkboxName].checked!=intendedValue){
                    postGetQueue.push(()=>toggleBox(key, url, Number(intendedValue)));
                    toToggleBack.push(()=>toggleBox(key, url, Number(!intendedValue)));
                }
            };
            let LAscript = $("#am_widget_Farm", doc).find("script")[0];
            if(!LAscript){
                UI.ErrorMessage("Loot assistant not activated, or some other error, hiding only player villages");
                hideVillages();
            }

            let urls = $("#am_widget_Farm", doc).find("script")[0].innerHTML.match(/([^']+=toggle_[^']+)/g);
            // console.log(urls);
            
            setToggleFunction("all_village_checkbox","all_villages", urls[0], false);
            setToggleFunction("full_losses_checkbox","full_losses", urls[1], true);
            setToggleFunction("partial_losses_checkbox","partial_losses", urls[2], true);
            setToggleFunction("attacked_checkbox","show_attacked", urls[3], true);
            setToggleFunction("full_hauls_checkbox", "only_full_hauls", urls[4], false);

            console.log(postGetQueue);
            executeQueue(postGetQueue, 300, {loadText:"toggling LA options", callback:()=>getBarbsInLA(0)});
        }).fail(()=>{UI.ErrorMessage("Couldn't load first LA page, will just hide player villages"); hideVillages();});
}

async function getBarbsInLA(page, depth=0, npages=undefined) {
    console.log("getBarbsInLA", page, depth, npages);
    let url = `/game.php?${sitter?sitter+"&":""}village=${game_data.village.id}&screen=am_farm&Farm_page=${page}`;
    $.get(url, async (data)=> {
        console.log("success");
        const parser = new DOMParser();
        const doc= await parser.parseFromString(data, "text/html");
        // console.log($(".paged-nav-item:last", doc));
        const pageSelector = $(".paged-nav-item:last", doc); 
        const npagesLA = parseInt(pageSelector.length? pageSelector[0].innerText.match(/\d+/g)[0]:0);
        // const npagesLA = parseInt($(".paged-nav-item:last", doc)[0].innerText.match(/\d+/g)[0]);
        let rows = $("#plunder_list", doc).find("tr[id^=village_]");
        if(rows.length){
            LA_ids = LA_ids.concat($.map(rows, function(obj){
                return obj.id.match(/\d+/g)[0];
            }));
        }
        // console.log(rows);
        // console.log(JSON.stringify(LA_ids));

        if(!npages){
            let pageQueue=[];
            for(var i = 1; i < npagesLA; i++){
                const j =i;
                pageQueue.push(()=>getBarbsInLA(j,0,npagesLA));// jshint ignore:line
            }
            // console.log(pageQueue);
            executeQueue(pageQueue, 300, {loadText:"loading LA pages",callback:()=>{
                hideVillages();
                executeQueue(toToggleBack, 300, {loadText:"toggling LA options back", callback: ()=>{toToggleBack = [];loadingLAstuff=false;}});
            }});

        }
    }).fail(()=>{
        if(depth < depthMax){
            UI.ErrorMessage(`Failed getting page ${page} of LA for the ${depth} time, will try again`);
            console.log(`Failed getting page ${page} of LA for the ${depth} time, will try again`);
            getBarbsInLA(page, depth +1, npages);
        }
        else{
            UI.ErrorMessage(`Failed getting page ${page} of LA for the ${depth} time, will not try again, getting next page`);
            console.log(`Failed getting page ${page} of LA for the ${depth} time, will try again`);
            getBarbsInLA(page +1, depth +1, npages);

        }
    });
}

main();