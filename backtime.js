/******PROGRAM VARS**********/
var scriptName = "Auto backtime";
var scriptTag = "fmBacktime";
/******PROGRAM VARS**********/
/*globals TWMap, Dialog, CommandPopup*/

function main(){
    if($(`#${scriptTag}_popup_container`).length){
        UI.ErrorMessage("Skript již byl načten, před opětovným voláním stránku znovu načtěte");
        return;
    }
    setHTML();
    backtimeSetup();
}

function setHTML(){
    let html =`
    <div id="${scriptTag}_popup_container" class="fm_popup_container">
        <div>
            <a class="popup_box_close tooltip-delayed" id="${scriptTag}_popup_cross" href="javascript:void(0)">
            </a>
            <div id="${scriptTag}_popup_content" class="fm_popup_content">
                <h3 class ="fm_centered">${scriptName}</h3>
                <span class = "fm_centered">
                    Spustí se <b>${window.templatename}</b> za <b>${window.backtimeTarget}</b> v <b class="${scriptTag}commandTimer" data-unit="axe" data-format="%hh%:%mm%:%ss%" data-endtime=${window.endtime}></b>
                </span>
            </div>
        </div>
    </div>
    <style>
        .fm_popup_container {
            border: 19px solid #804000;
            -moz-border-image: url("/graphic/popup/border.png") 9 19 19 19 repeat;
            -webkit-border-image: url("/graphic/popup/border.png") 9 19 19 19 repeat;
            -o-border-image: url("/graphic/popup/border.png") 19 19 19 19 repeat;
            border-image: url("/graphic/popup/border.png") 19 19 19 19 repeat;
            display: block;
            position: fixed;
            top: 8%;
            left: 70%;
            z-index: 14000;
        }
        .fm_popup_content {
            min-width: 200px;
            min-height: 50px;
            height:100%;
            background-image: url('/graphic/popup/content_background.png');
        }
        .fm_centered {
            text-align: center;
        }
    </style>`;
    $("body").append(html);
    $(`#${scriptTag}_popup_container`).draggable();
    $(`#${scriptTag}_popup_cross`).click(closePopup);

    initiateTimers(`.${scriptTag}commandTimer`, {
        refreshTime:300,
        htmlWrapper:(timeLeft)=>{
            if(timeLeft!="end")
                return `(<span style="color: green;">${timeLeft}</span>)`;
            else
                return `(<span style="color: red;">00:00:00</span>)`;
        }
    });
}

function closePopup(){
    $(`#${scriptTag}_popup_container`).remove();
}

function initiateTimers(selector, {htmlWrapper=(timeLeft)=>timeLeft, callback=()=>{},refreshTime=200}){
    setInterval(()=>{
        $(selector).html(function(index, content){
            if(!this.classList.contains(`${scriptTag}commandTimer`)){
                return content;
            }
            let format =this.dataset.format;
            if(!format){
                format = "%hh%:%mm%:%ss%";
            }
            let formattedTimeLeft = formatDifferenceTime(parseInt(this.dataset.endtime), Timing.getCurrentServerTime(), format);
            callback(formattedTimeLeft);
            return htmlWrapper(formattedTimeLeft);
        });
    }, refreshTime);
}
//Options for format: %M%, %D%, %h%, %m%, %s%, %ms%, ammount of repetitions accounts for padding
function formatDifferenceTime(newDate, oldDate, format="%hh%:%mm%:%ss%:%msmsms%", tooLateReturn="end") {
    let leftDate = newDate;
    let rightDate = oldDate;
    if (!(newDate instanceof Date))
        leftDate = new Date(newDate);
    if (!(newDate instanceof Date))
        rightDate = new Date(oldDate);

    let pad = (number, padLen) => number < 10**padLen?`${"0".repeat(padLen)}${number}`.slice(-padLen) : `${number}`;
    const msDif = leftDate - rightDate;
    if(msDif < 0)
        return tooLateReturn;
    const msIns = 1000;
    const msInm = 60 * msIns;
    const msInh = 60 * msInm;
    const msInD = 24 * msInh;
    const msInM = 30 * msInD;

    let monthRemainder = Math.floor(msDif / msInM);
    let dayRemainder = Math.floor(msDif % msInM / msInD);
    let hourRemainder = Math.floor(msDif % msInD / msInh);
    let minRemainder = Math.floor(msDif % msInh / msInm);
    let secondRemainder = Math.floor(msDif % msInm / msIns);
    let msRemainder = Math.floor(msDif % msIns);

    if(!format.match(/%(M)+%/g))
        dayRemainder += monthRemainder*30;
    if(!format.match(/%(D)+%/g))
        hourRemainder += dayRemainder*24;
    if(!format.match(/%(h)+%/g))
        minRemainder += hourRemainder*60;
    if(!format.match(/%(m)+%/g))
        secondRemainder += minRemainder*60;
    if(!format.match(/%(s)+%/g))
        msRemainder += secondRemainder*1000;

    let formattedReturn = format;
    $.each({"M":monthRemainder,"D":dayRemainder, "h":hourRemainder, "m":minRemainder, "s":secondRemainder, "ms":msRemainder},
        (key, obj) =>{
            let regexp = RegExp(`%(${key})+%`,"g");
            let objMatches = format.match(regexp);
            if(objMatches){
                $.each(objMatches, (ind, match)=>{
                    let charRegExp = RegExp(`${key}`,"g");
                    let len=match.match(charRegExp).length;
                    formattedReturn=formattedReturn.replace(match, pad(obj, len));
                });
            }
        });
    return formattedReturn;
}

function resizeMap(target, origin){
    let mapSize = TWMap.size;
    let distanceVector = [Math.abs(target[0]-origin[0]), Math.abs(target[1]-origin[1])];
    let mapMinusDistance= [mapSize[0]-distanceVector[0], mapSize[1]-distanceVector[1]];
    console.log(distanceVector);
    console.log(mapMinusDistance);
    if(Math.min(...mapMinusDistance) < 0){
        UI.ErrorMessage("Mapa je příliš malá");
        TWMap.resize(2 * Math.max(...distanceVector));
        return 3000;
    }
    return 1;
}

function backtimeSetup(){
    let target = window.backtimeTarget.split("|").map((val)=>parseInt(val));
    let origin = [game_data.village.x, game_data.village.y];

    let timeOut = resizeMap(target, origin);

    let templates = $.grep(Object.values(TWMap.troop_templates), (template)=> template.name==window.templatename);
  
let templates1 = $.grep(Object.values(TWMap.troop_templates), (template)=> template.name==window.templatename);
let troopTemp1 = templates1[0];
console.log(troopTemp1)

    let troopTemp = templates[0];
    setTimeout(()=>sendAttack(target,troopTemp), timeOut);
}

function sendAttack(target, troopTemp){
    let sendTime = window.endtime - Timing.getCurrentServerTime();
    if(sendTime < 0){
        UI.ErrorMessage("Čas odeslání již uplynul :(");
        return;
    }

    let n=Object.create(TWMap.current_units);
    if("all"!=troopTemp.id){
        let o=TWMap.troop_templates[troopTemp.id];
        for(let s in n){
            n[s]=o[s];
        }
        o.use_all.forEach(function(e){n[e]=TWMap.current_units[e];});
    }
    let l={template_id:troopTemp.id,source_village:TWMap.currentVillage,x:target[0],y:target[1],input:""};
    $.extend(l,n);
    l.attack=1;
    l[TWMap.command_hash[0]]=TWMap.command_hash[1];

    let p=function(e){
        e.preventDefault();let a=$("#command-data-form").serializeArray();
        TribalWars.post("place",{ajaxaction:"popup_command"}, a, function(e)
        {
            Dialog.close();
            UI.SuccessMessage(e.message);
            for(let a=0;a<CommandPopup.command_sent_hooks.length;a++)CommandPopup.command_sent_hooks[a](e);
            !function(){
                for(let e in n){
                    TWMap.current_units[e]-=n[e];
                }
            }();
        });
    };

    let XY = 1000*target[0]+target[1];
    let targetVillage =TWMap.villages[XY];
    let Mypopup = TWMap.popup;
    let villageid = targetVillage.id;
    let s=Mypopup._cache[villageid];
    if(void 0===s)
        Mypopup.loadVillage(villageid);
    setTimeout(function()
    {
        TribalWars.post("place",{village:TWMap.currentVillage,ajax:"confirm"},l,function(e){
            Dialog.show("confirm_attack",e.dialog);
            $("#command-data-form").on("submit",p);
            $("#troop_confirm_back").on("click",CommandPopup.goBack);
        });
    } , sendTime - 2000);
    setTimeout(function(){$("[id='troop_confirm_submit']")[0].click();}, sendTime);
    setTimeout(function(){
        window.close();
    }, sendTime + 1000);
}

main();