console.log('Starting map_farm');

var cookieName ="CoordsCookie"

function rdm(inf, sup) {
    return Math.round(Math.random()*(sup-inf) + inf);
}

function farmVillage (village) {
    console.log("Farming Village: ");
    console.log(village);

    var coordThis = TWMap.CoordByXY(village);
	var Mypopup= TWMap.popup; 
	var villageid = village.id;
	var s=Mypopup._cache[villageid]; 
	if(void 0===s) 
		Mypopup.loadVillage(villageid);

    // TWMap.mapHandler.onClick(coordThis[0], coordThis[1], new Event('click'));

    var url = TWMap.urls.ctx.mp_farm_a.replace(/__village__/, village.id).replace(/__source__/, game_data.village.id);

    setTimeout(function(){TribalWars.get(url, null, function (a) { TWMap.context.ajaxDone(null, url); }, undefined, undefined)},rdm(220, 260));
};

function getDist(village1, village2){
	var coords1, coords2;
    if(village1.xy){
		coords1 = TWMap.CoordByXY(village1.xy);
	}
	else{
		coords1 = [village1.x, village1.y];
	};

	if(village2.xy){
		coords2 = TWMap.CoordByXY(village2.xy);
	}
	else{
		coords2 = [village2.x, village2.y];	
	}

    return TWMap.context.FATooltip.distance(coords1[0], coords1[1], coords2[0], coords2[1]);
};

function getStorage(name) {
	var stored_ids = JSON.parse(window.localStorage.getItem(name) || "[]");
	console.log("stored_ids");
	console.log(stored_ids);
	return stored_ids;
}

function CookieFilter(barbs){
    var stored_ids=getStorage(cookieName);

    barbs = $.grep(barbs, function(e){
     return !(stored_ids.includes(e.id));
    });

    return barbs;
}


var radius = parseInt($("[id='map_chooser_select']")[0].value) / 2;

// radius*=1.2;

if(radius >= 15){radius= 1000};

console.log(radius)
var own_village = game_data.village;

console.log("radius:");
console.log(radius);

var villages = $.map(TWMap.villages, function(val, key) { return val; });
var barbs = $.grep(villages, function(e, i) {return e.owner=="0"});

console.log("villages: ")
console.log(villages);

console.log("own_village: ")
console.log(own_village);


barbs.forEach(function (e) {
    e.distance = getDist(e, own_village);
});

barbs = $.grep(barbs, function(e, i) {
    return e.distance < radius;
});

barbs = CookieFilter(barbs);

barbs = barbs.sort(function(a, b){return a.distance-b.distance});

console.log(barbs)

function two_distsfunc(barb1, barb2){
	return getDist(barb1, own_village) + getDist(barb1, barb2);
};

function two_dist_sort(barbs){
	var ret = [];
	var closest = barbs.shift();
	while (barbs.length> 0){
		ret.push(closest);
		var two_dists = $.map(barbs, function(obj, key){return two_distsfunc(obj, closest)});
		var min_two_dists = Math.min.apply(Math, two_dists);
		var index = two_dists.indexOf(min_two_dists);
		closest = barbs[index];
		barbs.splice(index, 1);
	}

	ret.push(closest);
	return ret;
}

barbs = two_dist_sort(barbs);

console.log(barbs);

for (const [index, barb] of barbs.entries()) {
    setTimeout(function(){farmVillage(barb)}, index* 500 + rdm(0, 300));
};